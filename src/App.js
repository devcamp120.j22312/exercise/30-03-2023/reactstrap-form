import 'bootstrap/dist/css/bootstrap.min.css';
import { Container, Row, Col } from 'reactstrap'

function App() {
  return (
    <div>
      <Container>
        <Row className='mt-3'>
          <Col className="text-center">
            <h2>HỒ SƠ NHÂN VIÊN</h2>
          </Col>
        </Row>
        <Row>
          <Col className='' xs="9">
            <Row className='  mt-3'>
              <Col xs="3" className=' '>
                Họ và tên
              </Col>
              <Col sx="6" className=' '>
                <input className='form-control' />
              </Col>
            </Row>
            <Row className='  mt-3'>
              <Col xs="3" className=' '>
                Ngày sinh
              </Col>
              <Col sx="6" className=' '>
                <input className='form-control' />
              </Col>
            </Row>
            <Row className='  mt-3'>
              <Col xs="3" className=' '>
                Số điện thoại
              </Col>
              <Col sx="6" className=' '>
                <input className='form-control' />
              </Col>
            </Row>
            <Row className='  mt-3'>
              <Col xs="3" className=' '>
                Giới tính
              </Col>
              <Col sx="6" className=' '>
                <input className='form-control' />
              </Col>
            </Row>
          </Col>
          <Col className='bg-light border' xs="3">
            Avatar
          </Col>
        </Row>
        <Row className='  mt-3'>
          <Col className='' xs="2">
            Công việc
          </Col>
          <Col xs="9">
            <input style={{marginLeft: "20px", width:"770px"}} className='form-control' />
          </Col>
        </Row>
        <Row className='mt-3' style={{marginLeft: "650px"}}>
          <Col>
            <button className='btn btn-success m-1'>Chi tiết</button>
            <button className='btn btn-success m-1'>Kiểm tra</button>
          </Col>
        </Row>

      </Container>
    </div>
  );
}

export default App;
